#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Pixel Pixel;

struct Pixel{
    unsigned char code[3];
};

// Getters
int getR(Pixel *pixel);
int getG(Pixel *pixel);
int getB(Pixel *pixel);
int getShade(Pixel *pixel);

// Setters
void setR(Pixel *pixel, unsigned char number);
void setG(Pixel *pixel, unsigned char number);
void setB(Pixel *pixel, unsigned char number);
void setShade(Pixel *pixel, int number);
