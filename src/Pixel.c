#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Pixel.h"

// Private function
int getNumberById(Pixel *pixel, int id){
    return (int) pixel->code[id];
}

#define getNumber getNumberById

// Getters

int getR(Pixel *pixel){
    return getNumber(pixel, 0);
}
int getG(Pixel *pixel){
    return getNumber(pixel, 1);
}

int getB(Pixel *pixel){
    return getNumber(pixel, 2);
}

int getShade(Pixel *pixel){
    return getNumber(pixel, 0) + getNumber(pixel, 1) + getNumber(pixel, 2);
}

#undef getNumber

// Setters

void setR(Pixel *pixel, unsigned char number){
    pixel->code[0] = number;
}
void setG(Pixel *pixel, unsigned char number){
    pixel->code[1] = number;
}
void setB(Pixel *pixel, unsigned char number){
    pixel->code[2] = number;
}
void setShade(Pixel *pixel, int number){
    if (number > 765 || number < 0) return;
    
    if (number <= 255) pixel->code[0] = (unsigned char) number;
    else if (number > 255 && number <= 510){
        pixel->code[0] = 255;
        number -= 255;
        pixel->code[1] = number;
    } else {
        pixel->code[0] = 255;
        number -= 255;
        pixel->code[1] = 255;
        number -= 255;
        pixel->code[2] = number;
    }
}