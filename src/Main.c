#include <stdio.h>
#include <string.h>
#include "Pixel.h"

int main(void){
    Pixel pix;
    
    setR(&pix, 255); 
    setG(&pix, 5);
    setB(&pix, 99);
    
    printf("Shade: %d \n", getShade(&pix));
    printf("R: %d ", getR(&pix));
    printf("G: %d ", getG(&pix));
    printf("B: %d \n", getB(&pix));
    printf("Size of pixel %lu Bytes \n", sizeof(Pixel));
    printf("Size of 3 integers %lu Bytes \n", 3*sizeof(int));
}